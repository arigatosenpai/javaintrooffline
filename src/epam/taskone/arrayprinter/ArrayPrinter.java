package epam.taskone.arrayprinter;

public class ArrayPrinter {

    public static void print(double[] array) {
        for (double value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

    public static void print(Double[] array) {
        for (Double value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }
}
