package epam.taskone.reader;

import epam.taskone.customvalidator.CustomValidator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ReaderTxt {
    public List<Double[]> reading() {
        try {
            final String PATH = "resources\\text.txt";
            String line;
            String[] arrayStr;
            List<Double[]> listArraysDouble = new ArrayList<>();

            List<String> listOfArrays = Files.readAllLines(Paths.get(PATH), StandardCharsets.UTF_8);
            ListIterator<String> iterator = listOfArrays.listIterator();
            while (iterator.hasNext())
            {
                line = iterator.next().trim();
                if (!CustomValidator.stringValidating(line)) { //false
                    iterator.remove();
                } else {
                    arrayStr = line.split("\\s+");
                    Double[] arrayDouble = new Double[arrayStr.length];

                    for (int i = 0; i < arrayStr.length; i++) {
                        arrayDouble[i] = Double.parseDouble(arrayStr[i]);
                    }
                    listArraysDouble.add(arrayDouble);
                }

            }
            return listArraysDouble;
        } catch (IOException ie) {
            System.out.println(ie);
        } finally {

        }
        return null;
    }
}
