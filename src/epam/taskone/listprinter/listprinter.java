package epam.taskone.listprinter;

import epam.taskone.arrayprinter.ArrayPrinter;

import java.util.List;
import java.util.ListIterator;

public class listprinter {
    public static void print(List<Double[]> arrayList) {
        ListIterator<Double[]> iterator = arrayList.listIterator();
        while (iterator.hasNext())
        {
            ArrayPrinter.print(iterator.next());
        }
    }
}
