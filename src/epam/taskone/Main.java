package epam.taskone;

import epam.taskone.reader.ReaderTxt;
import epam.taskone.arraycreator.ArrayCreator;
import epam.taskone.arrayprinter.ArrayPrinter;
import epam.taskone.arraysorter.ArraySorter;
import epam.taskone.arrayutil.ArrayUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.DoubleStream;

import static epam.taskone.listprinter.listprinter.print;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        double array[] = ArrayCreator.arrayCreation(10);

        ArrayUtil arrayUtil = new ArrayUtil();
        System.out.print("generated: ");
        ArrayPrinter.print(array);
        System.out.println("Minimal value in array: " + arrayUtil.arrayMin(array));
        System.out.println("Maximal value in array: " + arrayUtil.arrayMax(array));

        ArraySorter arraySorter = new ArraySorter();

        System.out.print("Sorted ascending: ");
        ArrayPrinter.print(arraySorter.bubbleSortingAsc(array));

        System.out.print("Sorted descending: ");
        ArrayPrinter.print(arraySorter.bubbleSortingDesc(array));

        System.out.print("Quick Sorted descending: ");
        arraySorter.quickSortingAsc(array);
        ArrayPrinter.print(array);

        System.out.print("Insertion sorted descending: ");
        ArrayPrinter.print(arraySorter.insertionSortingAsc(array));

        System.out.println("Average: " + arrayUtil.arrayAvg(array));
        System.out.println("Sum of elements: " + arrayUtil.sumElements(array));

        ReaderTxt reader = new ReaderTxt();
        ArrayList<Double[]> listOfArrays = new ArrayList<>(reader.reading());
        print(listOfArrays);

        DoubleStream stream = DoubleStream.of(2.8, 3.3, 0.1);
        stream.sorted().forEach(System.out::println);
    }
}
