package epam.taskone.arraycreator;

import java.util.Random;

public class ArrayCreator {

    public static double[] arrayCreation() {
        double[] array = new double[10];

        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.round((10 * random.nextDouble() + 0) * 100) ;
        }
        return array;
    }

    public static double[] arrayCreation(int size) {
        double[] array = new double[size];

        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = Math.round((10 * random.nextDouble() + 0) * 100) ;
        }
        return array;
    }
}
