package epam.taskone.arraysorter;

public class ArraySorter {

    public double[] bubbleSortingAsc(double[] array) { //sorting in ascending
        boolean sortingStatus; //= true

        do {
            sortingStatus = true;

            for (int j = 1; j < array.length; j++) {
                if (array[j] < array[j - 1]) {
                    exchange (array, j, j - 1);
                    sortingStatus = false;
                }
            }
        } while (!sortingStatus); //!= true
        return array;
    }

    public double[] bubbleSortingDesc(double[] array) { //sorting in Descending

        boolean sortingStatus; //= true
        do {
            sortingStatus = true;

            for (int j = 1; j < array.length; j++) {
                if (array[j] > array[j - 1]) {
                    exchange (array, j, j - 1);
                    sortingStatus = false;
                }
            }
        } while (!sortingStatus);
        return array;
    }

    public void quickSortingAsc(double[] array) {
        quickSort(array, 0, array.length - 1);
    }

    private void quickSort(double arr[], int begin, int end) {
        if (begin < end) {
            int partitionIndex = quickPartition(arr, begin, end);

            quickSort(arr, begin, partitionIndex - 1);
            quickSort(arr, partitionIndex + 1, end);
        }
    }

    private int quickPartition(double arr[], int begin, int end) {
        double pivot = arr[end];
        int i = (begin - 1);

        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;

                double swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }
        }

        double swapTemp = arr[i + 1];
        arr[i + 1] = arr[end];
        arr[end] = swapTemp;

        return i + 1;
    }

    public double[] insertionSortingAsc(double array[]) {
        for (int i = 1; i < array.length; i++) {
            for (int j = 0; j < i; j++) {
                if (array[i] <= array[j]) {
                    int tempI = i;
                    while (tempI > j) {
                        exchange (array, tempI, tempI - 1);
                        tempI--;
                    }
                }
            }
        }
        return array;
    }

    private void exchange(double[] array, int i, int j) {
        array[j] += array[i];
        array[i] = array[j] - array[i];
        array[j] -= array[i];
    }

}
