package epam.taskone.arrayutil; //ArrayAvg + MaxMin +

import java.util.stream.DoubleStream;

public class ArrayUtilStream {

    public double arrayAvg(double[] array) {
        return DoubleStream.of(array).average().getAsDouble();
    }

    public double arrayMax(double[] array) {
        return DoubleStream.of(array).max().getAsDouble();
    }

    public double arrayMin(double[] array) {
        return DoubleStream.of(array).min().getAsDouble();
    }

    public double sumElements(double[] array) {
        return DoubleStream.of(array).sum();
    }
}
