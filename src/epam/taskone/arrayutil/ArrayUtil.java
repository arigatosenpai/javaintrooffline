package epam.taskone.arrayutil; //ArrayAvg + MaxMin +

public class ArrayUtil {

    public double arrayAvg(double[] array) {
        double result = 0;

        for (double value : array) {
            result += value;
        }
        return result / array.length;
    }

    public double arrayMax(double[] array) {

        double maxValue = array[0];

        for (double k : array) {
            maxValue = (maxValue < k) ? k : maxValue;
        }

        return maxValue;
    }

    public double arrayMin(double[] array) {
        double minValue = array[0];

        for (double k : array) {
            minValue = (minValue > k) ? k : minValue;
        }
        return minValue;
    }

    public double sumElements(double[] array) {
        double sum = 0;

        for (double value : array) {
            sum += value;
        }
        return sum;
    }
}
