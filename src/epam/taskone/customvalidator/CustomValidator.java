package epam.taskone.customvalidator;

import java.util.regex.Pattern;

public class CustomValidator {
    public static boolean stringValidating(String str) {
        final String regexp = "(([\\-\\+]?[0-9]+\\.[0-9]+)\\s*)*";
        return Pattern.matches(regexp, str);
    }
}
