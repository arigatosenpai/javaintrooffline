package epam.taskone.arraysorter;

import epam.taskone.arraysorter.ArraySorter;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ArraySorterTest {

    @Test
    public void testBubbleSortingAsc() {
        double[] array = {5, 4, 4, 3, 1, 2};
        boolean sortStatus = true;

        ArraySorter arraySorter = new ArraySorter();
        double[] actualResult = arraySorter.bubbleSortingAsc(array);
        for(int i = 1; i < array.length; i++) {
            if(actualResult[i-1] > actualResult[i]){
                sortStatus = false;
                break;
            }
        }
        assertTrue (sortStatus);
    }

    @Test
    public void testBubbleSortingDesc() {
        double[] array = {5, 4, 4, 3, 1, 2};
        boolean sortStatus = true;

        ArraySorter arraySorter = new ArraySorter();
        double[] actualResult = arraySorter.bubbleSortingDesc(array);
        for(int i = 1; i < array.length; i++) {
            if(actualResult[i-1] < actualResult[i]){
                sortStatus = false;
                break;
            }
        }
        assertTrue (sortStatus);
    }


    @Test
    public void testQuickSortingAsc() {
    }

    @Test
    public void testExchange() {
    }
}