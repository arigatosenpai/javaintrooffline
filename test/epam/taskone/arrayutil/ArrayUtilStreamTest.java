package epam.taskone.arrayutil;


import org.testng.annotations.*;
import static org.testng.Assert.*;

public class ArrayUtilStreamTest {

    ArrayUtil au = new ArrayUtil();

    @DataProvider(name = "dataProvider")
    public Object[][] dataProvider() {
        return new Object[][] {
                {new double[]{1.2, 2.2, 3.2, 0.1}, 1.675, 3.2, 0.1, 6.7}, /*avg, max, min */

        };
    }

    @Test(dataProvider = "dataProvider")
    public void testArrayAvg(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        double actualResult = au.arrayAvg(array);
        assertEquals(actualResult, expectedAvg); //expectedResult
    }

    @Test(dataProvider = "dataProvider")
    public void testArrayMax(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        double actualResult = au.arrayMax(array);
        assertEquals(actualResult, expectedMax);
    }

    @Test(dataProvider = "dataProvider")
    public void testArrayMin(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        double actualResult = au.arrayMin(array);
        assertEquals(actualResult, expectedMin);
    }

    @Test(dataProvider = "dataProvider")
    public void testSumElements(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        double actualResult = au.sumElements(array);
        assertEquals(actualResult, expectedSum);
    }
}