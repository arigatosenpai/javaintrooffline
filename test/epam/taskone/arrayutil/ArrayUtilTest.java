package epam.taskone.arrayutil;

import static org.testng.Assert.*;
import org.testng.annotations.*;

public class ArrayUtilTest {

    @DataProvider(name = "dataProvider")
    public Object[][] dataProvider() {
        return new Object[][] {
                {new double[]{1.2, 2.2, 3.2, 0.1}, 1.675, 3.2, 0.1, 6.7}, /*avg, max, min */

        };
    }
    @Test(dataProvider = "dataProvider")
    public void testArrayAvg(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        /*double[] array = {1.2, 2.2, 3.2};
        double expectedResult = 2.2;*/

        ArrayUtil au = new ArrayUtil();
        double actualResult = au.arrayAvg(array);
        assertEquals(actualResult, expectedAvg); //expectedResult
    }

    @Test(dataProvider = "dataProvider")
    public void testArrayMax(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        /*double[] array = {1.2, 2.2, 3.2, 0.1};
        double expectedResult = 3.2;*/

        ArrayUtil au = new ArrayUtil();
        double actualResult = au.arrayMax(array);
        assertEquals(actualResult, expectedMax);
    }

    @Test(dataProvider = "dataProvider")
    public void testArrayMin(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        /*double[] array = {1.2, 2.2, 3.2, 0.1};
        double expectedResult = 0.1;*/

        ArrayUtil au = new ArrayUtil();
        double actualResult = au.arrayMin(array);
        assertEquals(actualResult, expectedMin);
    }

    @Test(dataProvider = "dataProvider")
    public void testSumElements(double[] array, double expectedAvg, double expectedMax, double expectedMin, double expectedSum) {
        /*double[] array = {1.2, 2.2, 3.2, 0.1};
        double expectedResult = 3.2;*/

        ArrayUtil au = new ArrayUtil();
        double actualResult = au.sumElements(array);
        assertEquals(actualResult, expectedSum);
    }
}